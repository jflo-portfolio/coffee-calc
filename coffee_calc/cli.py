#!/usr/bin/env python3
import argparse
from typing import Tuple


def coffee_calc(
    ratio: str, coffee: float = 0, water: float = 0
) -> Tuple[float, float, float]:
    """calculate and display coffee/water amounts

    :param ratio: coffee to water ratio
    :type ratio: str
    :param coffee: amount of coffee in grams
    :type coffee: float
    :param water: amount of water in milliliters
    :type water: float
    :return: the amount of water, coffee and brewed_coffee
    :rtype: Tuple of floats
    """
    allowed_ratios = ["1:13", "1:14", "1:15", "1:16", "1:17", "1:18", "1:19"]
    if ratio not in allowed_ratios:
        raise AttributeError(
            "Incorrect ratio. Must be one of {0}".format(allowed_ratios)
        )

    if water == 0 and coffee == 0:
        raise AttributeError("Values for either coffee, or water must be supplied")

    ratio_denominator: int = int(ratio.split(":")[1])

    if coffee:
        water = coffee * ratio_denominator
    elif water:
        coffee = water / ratio_denominator

    brewed_coffee = water - (coffee * 1.995)

    return (round(water, 2), round(coffee, 2), round(brewed_coffee, 2))


def main() -> None:
    """Script to calculate coffee/water amounts depending on ratio"""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-r",
        "--ratio",
        type=str,
        default="1:16",
        action="store",
        help="ratio",
    )
    parser.add_argument(
        "-c",
        "--coffee",
        type=float,
        default=0,
        action="store",
        help="coffee",
    )
    parser.add_argument(
        "-w",
        "--water",
        type=float,
        default=500,
        action="store",
        help="water",
    )

    args = parser.parse_args()

    water, coffee, brewed_coffee = coffee_calc(args.ratio, args.coffee, args.water)

    print("{} coffee to water ratio".format(args.ratio))
    print("{:.2f} milliliters of water".format(water))
    print("{:.2f} grams of coffee".format(coffee))
    print("produces {:.2f} milliliters of brewed coffee".format(brewed_coffee))


if __name__ == "__main__":
    main()
