from typing import Tuple

import pytest

from coffee_calc import __version__
from coffee_calc import cli


def test_version() -> None:
    assert __version__ == "0.1.0"


@pytest.mark.parametrize(
    "ratio, result",
    [
        ("1:19", (500, 26.32, 447.5)),
        ("1:18", (500, 27.78, 444.58)),
        ("1:17", (500, 29.41, 441.32)),
        ("1:16", (500, 31.25, 437.66)),
        ("1:15", (500, 33.33, 433.5)),
        ("1:14", (500, 35.71, 428.75)),
        ("1:13", (500, 38.46, 423.27)),
    ],
)
def test_coffee_calc_ratio(ratio: str, result: Tuple[float, float, float]) -> None:
    w, c, bc = cli.coffee_calc(ratio=ratio, coffee=0, water=500)
    assert w == result[0]
    assert c == result[1]
    assert bc == result[2]


@pytest.mark.parametrize(
    "water, result",
    [
        (250, (250, 15.62, 218.83)),
        (300, (300, 18.75, 262.59)),
        (400, (400, 25.0, 350.12)),
        (500, (500, 31.25, 437.66)),
        (750, (750, 46.88, 656.48)),
    ],
)
def test_coffee_calc_water(water: float, result: Tuple[float, float, float]) -> None:
    w, c, bc = cli.coffee_calc(ratio="1:16", coffee=0, water=water)
    assert w == result[0]
    assert c == result[1]
    assert bc == result[2]


@pytest.mark.parametrize(
    "coffee, result",
    [
        (25, (400, 25, 350.12)),
        (30, (480, 30, 420.15)),
        (35, (560, 35, 490.18)),
        (40, (640, 40, 560.2)),
        (45, (720, 45, 630.23)),
        (50, (800, 50, 700.25)),
    ],
)
def test_coffee_calc_coffee(coffee: float, result: Tuple[float, float, float]) -> None:
    w, c, bc = cli.coffee_calc(ratio="1:16", coffee=coffee, water=0)
    assert w == result[0]
    assert c == result[1]
    assert bc == result[2]


@pytest.mark.parametrize(
    "ratio, coffee, water, result",
    [
        ("1:17", 50, 0, (850, 50, 750.25)),
        ("1:17", 0, 450, (450, 26.47, 397.19)),
        ("1:16", 25, 0, (400, 25, 350.12)),
        ("1:16", 0, 450, (450, 28.12, 393.89)),
        ("1:15", 30, 0, (450, 30, 390.15)),
        ("1:15", 0, 450, (450, 30, 390.15)),
    ],
)
def test_coffee_calc_combo(
    ratio: str, coffee: float, water: float, result: Tuple[float, float, float]
) -> None:
    w, c, bc = cli.coffee_calc(ratio=ratio, coffee=coffee, water=water)
    assert w == result[0]
    assert c == result[1]
    assert bc == result[2]


def test_coffee_calc_no_water_coffee() -> None:
    with pytest.raises(AttributeError):
        cli.coffee_calc(ratio="1:16", coffee=0, water=0)


def test_coffee_calc_incorrect_ratio() -> None:
    with pytest.raises(AttributeError):
        cli.coffee_calc(ratio="1:10", coffee=0, water=500)
