# coffee-calc

coffee-calc is a Python command line program to calculate the amount of coffee or water required given a ratio.


## Installation

### Python version

Python (>= 3.7)

### Install coffee-calc

    pip install git+https://bitbucket.org/jflo-portfolio/coffee-calc.git

## Usage

type coffee-calc:

    $ coffee-calc

this will use with the default ratio of "1:16" and 500 millileters of water.
this should print out:
    
    1:16 coffee to water ratio
    500.00 milliliters of water
    31.25 grams of coffee
    produces 437.66 milliliters of brewed coffee

you can also specify the ratio, water or coffee amount:
    
    $ coffee-calc -r 1:15 -w 350

    $ coffee-calc -r 1:14 -c 32 